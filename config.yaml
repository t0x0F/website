baseurl: https://gitea.io/
languageCode: en-us
title: Gitea
theme: gitea

markup:
  defaultMarkdownHandler: blackFriday

defaultContentLanguage: en-us
defaultContentLanguageInSubdir: true
enableMissingTranslationPlaceholders: true

permalinks:
  post: /:year/:month/:title/
  doc: /:slug/
  page: /:slug/
  default: /:slug/

params:
  description: Git with a cup of tea
  author: The Gitea Authors
  website: https://gitea.io

menu:
  page:
    - name: Website
      url: /en-us/
      weight: 10
      pre: home
      post: active
    - name: Docs
      url: https://docs.gitea.io/en-us/
      weight: 20
      pre: question
    - name: API
      url: https://try.gitea.io/api/swagger
      weight: 45
      pre: plug
    - name: Blog
      url: https://blog.gitea.io/
      weight: 30
      pre: rss
    - name: Code
      url: https://code.gitea.io/
      weight: 40
      pre: code
    - name: Translation
      url: https://crowdin.com/project/gitea
      weight: 41
      pre: language
    - name: Downloads
      url: https://dl.gitea.io/
      weight: 50
      pre: download
    - name: GitHub
      url: https://github.com/go-gitea/
      weight: 60
      pre: github
    - name: Discord Chat
      url: https://discord.gg/Gitea
      weight: 70
      pre: comment
    - name: Forum
      url: https://discourse.gitea.io/
      weight: 80
      pre: group

languages:
  en-us:
    weight: 0
    languageName: English

  zh-cn:
    weight: 1
    languageName: 中文(简体)
    menu:
      page:
        - name: 网站
          url: /zh-cn/
          weight: 10
          pre: home
          post: active
        - name: 文档
          url: https://docs.gitea.io/zh-cn/
          weight: 20
          pre: question
        - name: API
          url: https://try.gitea.io/api/swagger
          weight: 45
          pre: plug
        - name: 博客
          url: https://blog.gitea.io/
          weight: 30
          pre: rss
        - name: 导入
          url: https://code.gitea.io/
          weight: 40
          pre: code
        - name: 翻译
          url: https://crowdin.com/project/gitea
          weight: 41
          pre: language
        - name: 下载
          url: https://dl.gitea.io/
          weight: 50
          pre: download
        - name: GitHub
          url: https://github.com/go-gitea/
          weight: 60
          pre: github
        - name: Discord Chat
          url: https://discord.gg/Gitea
          weight: 70
          pre: comment
        - name: Forum
          url: https://discourse.gitea.io/
          weight: 80
          pre: group

  zh-tw:
    weight: 2
    languageName: 中文(繁體)
    menu:
      page:
        - name: 網站
          url: /zh-tw/
          weight: 10
          pre: home
          post: active
        - name: 文件
          url: https://docs.gitea.io/zh-tw/
          weight: 20
          pre: question
        - name: API
          url: https://try.gitea.io/api/swagger
          weight: 45
          pre: plug
        - name: 部落格
          url: https://blog.gitea.io/
          weight: 30
          pre: rss
        - name: 程式碼
          url: https://code.gitea.io/
          weight: 40
          pre: code
        - name: 翻译
          url: https://crowdin.com/project/gitea
          weight: 41
          pre: language
        - name: 下载
          url: https://dl.gitea.io/
          weight: 50
          pre: download
        - name: GitHub
          url: https://github.com/go-gitea/
          weight: 60
          pre: github
        - name: Discord Chat
          url: https://discord.gg/Gitea
          weight: 70
          pre: comment
        - name: Forum
          url: https://discourse.gitea.io/
          weight: 80
          pre: group

  pt-br:
    weight: 3
    languageName: Português Brasileiro
    menu:
      page:
        - name: Página inicial
          url: /pt-br/
          weight: 10
          pre: home
          post: active
        - name: Documentação
          url: https://docs.gitea.io/pt-br/
          weight: 20
          pre: question
        - name: API
          url: https://try.gitea.io/api/swagger
          weight: 45
          pre: plug
        - name: Blog
          url: https://blog.gitea.io/
          weight: 30
          pre: rss
        - name: Código-fonte
          url: https://code.gitea.io/
          weight: 40
          pre: code
        - name: Translation
          url: https://crowdin.com/project/gitea
          weight: 41
          pre: language
        - name: Downloads
          url: https://dl.gitea.io/
          weight: 50
          pre: download
        - name: GitHub
          url: https://github.com/go-gitea/
          weight: 60
          pre: github
        - name: Chat no Discord
          url: https://discord.gg/Gitea
          weight: 70
          pre: comment
        - name: Forum
          url: https://discourse.gitea.io/
          weight: 80
          pre: group

  nl-nl:
    weight: 4
    languageName: Nederlands
    menu:
      page:
        - name: Website
          url: /nl-nl/
          weight: 10
          pre: home
          post: active
        - name: Docs
          url: https://docs.gitea.io/nl-nl/
          weight: 20
          pre: question
        - name: API
          url: https://try.gitea.io/api/swagger
          weight: 45
          pre: plug
        - name: Blog
          url: https://blog.gitea.io/
          weight: 30
          pre: rss
        - name: Code
          url: https://code.gitea.io/
          weight: 40
          pre: code
        - name: Translation
          url: https://crowdin.com/project/gitea
          weight: 41
          pre: language
        - name: Downloads
          url: https://dl.gitea.io/
          weight: 50
          pre: download
        - name: GitHub
          url: https://github.com/go-gitea/
          weight: 60
          pre: github
        - name: Discord Chat
          url: https://discord.gg/Gitea
          weight: 70
          pre: comment
        - name: Forum
          url: https://discourse.gitea.io/
          weight: 80
          pre: group

  ko-kr:
    weight: 5
    languageName: 한국어
    menu:
      page:
        - name: 웹사이트
          url: /ko-kr/
          weight: 10
          pre: home
          post: active
        - name: 문서
          url: https://docs.gitea.io/ko-kr/
          weight: 20
          pre: question
        - name: API
          url: https://try.gitea.io/api/swagger
          weight: 45
          pre: plug
        - name: 블로그
          url: https://blog.gitea.io/
          weight: 30
          pre: rss
        - name: 코드
          url: https://code.gitea.io/
          weight: 40
          pre: code
        - name: Translation
          url: https://crowdin.com/project/gitea
          weight: 41
          pre: language
        - name: 다운로드
          url: https://dl.gitea.io/
          weight: 50
          pre: download
        - name: GitHub
          url: https://github.com/go-gitea/
          weight: 60
          pre: github
        - name: Discord 채팅
          url: https://discord.gg/Gitea
          weight: 70
          pre: comment
        - name: 포럼
          url: https://discourse.gitea.io/
          weight: 80
          pre: group

  ja-jp:
    weight: 6
    languageName: 日本語
    menu:
      page:
        - name: ウェブサイト
          url: /ja-jp/
          weight: 10
          pre: home
          post: active
        - name: ドキュメント
          url: https://docs.gitea.io/ja-jp/
          weight: 20
          pre: question
        - name: API
          url: https://try.gitea.io/api/swagger
          weight: 45
          pre: plug
        - name: ブログ
          url: https://blog.gitea.io/
          weight: 30
          pre: rss
        - name: コード
          url: https://code.gitea.io/
          weight: 40
          pre: code
        - name: 翻訳
          url: https://crowdin.com/project/gitea
          weight: 41
          pre: language
        - name: ダウンロード
          url: https://dl.gitea.io/
          weight: 50
          pre: download
        - name: GitHub
          url: https://github.com/go-gitea/
          weight: 60
          pre: github
        - name: Discord チャット
          url: https://discord.gg/Gitea
          weight: 70
          pre: comment
        - name: フォーラム
          url: https://discourse.gitea.io/
          weight: 80
          pre: group
